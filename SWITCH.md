# Día de la semana

Programa utilizando SWITCH

#include <stdio.h>  
#include <stdlib.h> 
#include <math.h>

int main(){

int x;

printf("Introduce un número entre 1 y 4" );
scanf("%i", &x);

switch(x){ //El switch es como el if y se ponen casos.
//case = En el caso de que...
case 1 : printf("Lunes");break; //break sirve para parar un programa y que no ejecute todas las líneas
case 2 : printf("Martes");break;
case 3 : printf("Miércoles");break;
case 4 : printf("Jueves");break;
default : printf("El número es incorrecto"); //El default es como el else en el if
}
return EXIT_SUCESS; //O return 0 en windows    
}
